<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>


<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="user" style="background-color: lightcyan;">

	<header> </header>

	<form style="text-align: center" action="UserRegistServlet"
		method="post">
		<div>
			<font size="6">ユーザ新規登録</font>
		</div>
		<a class="headline1" href="Logoutservlet">
			<p style="text-align: right">
				<td align="right">ログアウト</td>
			</p>
		</a>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<div class="form-group col-sm-3 ">
			<label for="inputLogin_id" class=" col-form-label">ログインID</label> <input
				type="text" class="form-control" id="inputLoginId" name="loginId">
		</div>
		<div class="form-group col-sm-3">
			<label for="inputLogin_id" class="col-form-label">パスワード</label> <input
				type="password" class="form-control" id="inputPassword"
				name="loginPassword">
		</div>
		<div class="form-group col-sm-3">
			<label for="inputLogin_id" class="col-form-label">パスワード(確認)</label> <input
				type="password" class="form-control" id="inputRePassword"
				name="reLoginPassword">
		</div>
		<div class="form-group col-sm-3">
			<label for="inputLogin_id" class="col-form-label">ユーザ名</label> <input
				type="text" class="form-control" id="inputName" name="userName">
		</div>
		<div class="form-group col-sm-3">
			<label for="inputLogin_id" class="col-form-label">生年月日</label> <input
				type="date" class="form-control" id="inputBirth_date"
				name="birthDate">
		</div>
		<div class="center">
			<button type="submit" class="btn btn-primary col-sm-1">登録</button>
		</div>
		<a class="nav-link" href="UserListServlet">戻る</a>

	</form>

</body>

</html>
