<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title><link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board" style="background-color: lightcyan;">
	<div>
		<font size="6">投稿記事表示</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right">ログアウト</td>
		</p>
	</a>



	<table border="5">
		<tr>

			<th>ユーザ名</th>
			<th>タイトル</th>
			<th>タグ名</th>
			<th width="80%">本文</th>
			<th></th>
		</tr>

		<tr>
			<td>${board.name}</td>
			<td>${board.title}</td>
			<td>${board.tagName}</td>
			<td>${board.main}</td>

			<td></td>
		</tr>


		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>

	<table border="5">
		<tr>
			<th>ユーザ名</th>
			<th width="80%">コメント</th>
			<th></th>
		</tr>
		<c:forEach var="comment" items="${commentList}">
			<tr>
				<c:if test="${empty comment.cTocId}">
					<td>${comment.cName}</td>
					<td>${comment.cMain}</td>
					<td><a class="btn btn-primary"
						href="CommentMonitorServlet?id=${comment.cId}">参照</a><a
						class="btn btn-primary"
						href="CommentReMakeServlet?id=${comment.cId}">コメント</a>
						<c:if test="${userInfo.userId==comment.cUserId}">
						<a
						class="btn btn-success"
						href="CommentDeleteServlet?id=${comment.cId}">削除</a></c:if></td>
				</c:if>

			</tr>
		</c:forEach>



		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>
</body>

</html>
