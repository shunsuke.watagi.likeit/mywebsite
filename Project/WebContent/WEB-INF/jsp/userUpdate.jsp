<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>
<body class="user" style="background-color: lightcyan;">
	<div class="headline">
		<font size="5"> <a>${User.userName}さん</a></font>
	</div>
	<form style="text-align: center" action="UserUpdateServlet"
		method="post">
		<header> </header>
		<div class="center">
			<font size="6">ユーザ情報更新</font>
		</div>

		<a class="headline1" href="LogoutServlet"><p
				style="text-align: right">ログアウト</p></a>
		<ul class="nav-list">

		</ul>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">ログインID
				${User.loginId} </label>
		</div>
		<input type="hidden" name="userId" value="${User.userId}"> <input
			type="hidden" name="loginId" value="${User.loginId}">
		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">パスワード</label>
		</div>
		<div class="form-group col-sm-3">
			<input type="password" class="form-control" id="Password"
				name="loginPassword" value="loginPassword">
		</div>
		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">パスワード(確認)</label>
		</div>
		<div class="form-group col-sm-3">
			<input type="password" class="form-control" id="rePassword"
				name="reLoginPassword" value="reLoginPassword">
		</div>
		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">ユーザ名</label>
		</div>
		<div class="form-group col-sm-3">
			<input type="text" class="form-control" id="inputName"
				name="userName" value=${User.userName }>
		</div>

		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">生年月日</label>
		</div>
		<div class="form-group col-sm-3">
			<input type="date" class="form-control" id="inputBirthDate"
				name="birthDate" value=${User.birthDate }>
		</div>
		<div class="center">
			<button type="submit" class="btn btn-primary">更新</button>

		</div>
		<a class="nav-link" href="UserListServlet">戻る</a>
	</form>
	>
</body>
</html>
