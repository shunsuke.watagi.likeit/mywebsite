<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board" >
	<div>
		<font size="6">掲示板初期画面</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right" color="red">
			<td align="right">ログアウト</td>
		</p>
	</a>
	<div>
		<p style="text-align: right">
			<a class="nav-link" href="BoardMakeServlet">投稿</a> <a
				class="nav-link" href="BoardSearchServlet">検索</a>
		</p>
	</div>
	<table class="bL">
		<tr>

			<th>ユーザ名</th>
			<th width="80%">本文</th>
			<th></th>
		</tr>
		<c:forEach var="board" items="${boardList}">
			<tr>


				<td>${board.name}</td>
				<td>${board.main}</td>
				<td><a class="nav-link"
					href="BoardMonitorServlet?Id=${board.id}">参照</a> <a
					class="btn btn-primary" href="CommentMakeServlet?Id=${board.id}">コメント</a>
					<c:if test="${userInfo.userId==board.userId}">
						<a class="btn btn-success"
							href="BoardDeleteServlet?Id=${board.id}">削除</a>
					</c:if></td>
			</tr>

		</c:forEach>

		<tr>

		</tr>
	</table>
	<p style="text-align: right">
		<a class="nav-link" href="UserListServlet">ユーザー情報へ</a>
	</p>
</body>

</html>
