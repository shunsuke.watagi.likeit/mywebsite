<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="user">
	<header>
		<h1 class="headline">
			<a><font size="5">${userInfo.userName}さん</font></a>
		</h1>
		<div></div>

	</header>


	<form style="text-align: center" action="UserListServlet" method="post">
		<font size="6">ユーザ一覧</font> <a class="headline1" href="LogoutServlet">
			<p style="text-align: right">
				<font size="3">ログアウト</font>
			</p>
		</a>
		<ul class="nav-list">

		</ul>
		<div class="right">
			<a class="nav-link" href="UserRegistServlet">
				<p style="text-align: right">
					<font size="3">新規登録</font>
				</p>
			</a>
		</div>
		<div class="form-group">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">ログインID</label>

		</div>
		<div class="form-group col-sm-3" style="div-align: center">
			<input type="text" class="form-control" id="inputLogin_id"
				name="loginId">
		</div>




		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">ユーザ名</label>

		</div>
		<div class="form-group col-sm-3">
			<input type="text" class="form-control" id="inputLogin_id"
				name="userName" size="30">
		</div>




		<div class="form-group ">
			<label for="inputLogin_id" class="col-sm-2 col-form-label">生年月日</label>
		</div>


		<div class="form-group col-sm-3">
			<div class="col-sm-9 row">
				<input type="date" class="form-control col-sm-5" placeholder="年/月/日"
					name="birthDate"> ～ <input type="date"
					class="form-control col-sm-5" placeholder="年/月/日" name="birthDate2">
			</div>
		</div>
		<div>
			<button type="submit" class="btn btn-primary"
				style="WIDTH: 80px; HEIGHT: 30px">検索</button>
		</div>

	</form>

	<table border="5">
		<tr>
			<th>ログインID</th>
			<th>ユーザ名</th>
			<th>生年月日</th>
			<th></th>
		</tr>
		<c:forEach var="user" items="${userList}">
			<tr>
				<c:if test="${user.userId!=1}">
					<td>${user.loginId}</td>
					<td>${user.userName}</td>
					<td>${user.birthDate}</td>
					<!-- TODO 未実装；ログインボタンの表示制御を行う -->
					<td><c:if test="${userInfo.userId==1}">

							<a class="btn btn-primary"
								href="UserDetailServlet?userId=${user.userId}">詳細</a>
							<a class="btn btn-success"
								href="UserUpdateServlet?userId=${user.userId}">更新</a>
							<a class="btn btn-danger"
								href="UserDeleteServlet?userId=${user.userId}">削除</a>
						</c:if> <c:if test="${userInfo.userId!=1}">
							<a class="btn btn-primary"
								href="UserDetailServlet?userId=${user.userId}">詳細</a>
							<c:if test="${userInfo.userId==user.userId}">
								<a class="btn btn-success"
									href="UserUpdateServlet?userId=${user.userId}">更新</a>
							</c:if>
						</c:if></td>
				</c:if>
			</tr>

		</c:forEach>

		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>
</body>

</html>
