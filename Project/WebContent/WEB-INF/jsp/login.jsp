<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>



<head style="background-color:#FFFFEE">

<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="login">



	<form style="text-align: center" class="form-horizontal"
		action="LoginServlet" method="post">
		<h1>ログイン</h1>
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<div style="text-align: center" class="form-group col-sm-3">

			<label for="inputLogin_id" class="col-form-label">ログインID</label> <input
				type="text" class="form-control" id="inputLoginId"
				placeholder="ログインID" name="loginId" size="40" maxlength="32">

		</div>

		<div style="text-align: center" class="form-group col-sm-3">
			<label for="inputPassword" class="col-form-label">パスワード</label><input
				type="password" class="form-control" id="inputLoginPassword"
				placeholder="パスワード" name="loginPassword" size="40" maxlength="32">
		</div>
		<div class="center">
			<button type="submit" class="btn btn-primary">Login</button>
		</div>
		<a class="nav-link" href="UserRegistServlet">新規登録</a>
	</form>

</body>

</html>
