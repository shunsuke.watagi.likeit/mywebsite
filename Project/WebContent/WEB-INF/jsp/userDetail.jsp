<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="user" style="background-color: lightcyan;">
	<header> </header>
	<h1 class="headline">
		<font size="5"> <a>${User.userName}さん</a></font>
	</h1>

	<div><font size="6">ユーザ情報詳細参照</font></div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">ログアウト</p>
	</a>
	<ul class="nav-list">

	</ul>
	<div class="form-groups">
		<p>
			<label for="staticEmail" class="col-sm-2 col-form-label">ログインID:
				${User.loginId}</label>
		</p>
		<div class="col-sm-10"></div>
		<p>
			<label for="staticEmail" class="col-sm-2 col-form-label">ユーザ名:
				${User.userName}</label>
		</p>
		<div class="col-sm-10"></div>

		<p>
			<label for="staticEmail" class="col-sm-2 col-form-label">生年月日:
				${User.birthDate}</label>
		</p>

		<p>
			<label for="staticEmail" class="col-sm-2 col-form-label">登録日時:
				${User.createDate}</label>
		</p>
		<div class="col-sm-10"></div>

		<p>
			<label for="staticEmail" class="col-sm-2 col-form-label">更新日時:${User.updateDate}</label>
		</p>

		<a class="nav-link" href="UserListServlet">戻る</a>
	</div>
</body>
</html>
