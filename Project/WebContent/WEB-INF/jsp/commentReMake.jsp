<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title><link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous"><link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board" style="background-color: lightcyan;">
	<div>
		<font size="6">コメント作成機能</font>
	</div>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right">ログアウト</td>
		</p>
	</a>

	<table border="5">
		<tr>

			<th>ユーザ名</th>
			<th>タイトル</th>
			<th width="80%">本文</th>

		</tr>

		<tr>


			<td>${comment.cName}</td>
			<td>${comment.cTitle}</td>
			<td>${comment.cMain}</td>

		</tr>



		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>
	<form style="text-align:center" class="form-horizontal" action="CommentReMakeServlet"
		method="post">
		<div class="form-group col-sm-3">
			<p>ユーザ名</p>
			<input type="text" class="form-control" name="name" size="20" maxlength="20"
				value="${userInfo.userName}">
		</div>
		<div class="form-group col-sm-3" >
			<p>タイトル</p>
			<input type="text"class="form-control" name="title" size="20" maxlength="20">
		</div>
		<div class="form-group col-sm-3">
			<p>本文</p>
			<textarea type="text" class="form-control" name="main" rows="20"
				cols="50"> </textarea>
		</div>
		<input type="hidden" name="userId" value="${userInfo.userId}">
		<input type="hidden" name="boardId" value="${comment.id}"> <input
			type="hidden" name="cId" value="${comment.cId}">

		<div>
			<button type="submit" class="btn btn-primary col-sm-1"
				style="width: 6%;">投稿</button>
		</div>
		<a class="nav-link" href="BoardListServlet">キャンセル</a>
	</form>


</body>

</html>
