<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board" style="background-color: lightcyan;">
	<div>
		<font size="6">投稿記事削除画面</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right">ログアウト</td>
		</p>
	</a>
	<table border="5" width="50%">
		<tr>
			<th>ユーザー名</th>
			<th>タイトル</th>
			<th>本文</th>
		</tr>

		<tr>
			<td>${comment.cName}</td>
			<td>${comment.cTitle}</td>
			<td>${comment.cMain}</td>

		</tr>

	</table>
	<div><font size="5">この記事を削除してよろしいですか？</font></div>

	<table>
		<tr>
			<form action="CommentDeleteServlet" method="post">
				<td><button type="submit" class="btn btn-primary btn-lg"
						name="yes">はい</button></td> <input type="hidden" name="cId"
					value="${comment.cId}">
			</form>
			<form action="BoardListServlet" method="get">
				<td><button type="submit" class="btn btn-secondary btn-lg"
						name="no">いいえ</button></td>
			</form>
		</tr>
	</table>


</body>

</html>