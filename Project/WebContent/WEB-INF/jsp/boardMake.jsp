<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>


<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>


<body class="board">

	<div>
		<font size="6">掲示板</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right">ログアウト</td>
		</p>
	</a>
	<form class=" form-horizontal" action="BoardMakeServlet" method="post">


		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<div class="form-groups col-sm-3">
			<p>ユーザ名</p>
			<input type="text" name="name" class="form-control"
				value="${userInfo.userName}" size="20" maxlength="20">
		</div>
		<div class="form-groups col-sm-3">
			<p>タイトル</p>
			<input type="text" class="form-control" name="title" size="20"
				maxlength="20">
		</div>
		<div class="form-groups col-sm-3">
			<p>タグ名</p>
			<input type="text" class="form-control" name="tagName" size="20"
				maxlength="20">
		</div>
		<div class="form-groups col-sm-3">
			<p>本文</p>
			<textarea type="text" class="form-control" name="main" rows="20"
				cols="50"> </textarea>
		</div>
		<input type="hidden" name="userId" value="${userInfo.userId}">
		<div>
			<button type="submit" class="btn btn-primary col-sm-1"
				style="width: 6%;">投稿</button>
		</div>
		<a class="nav-link" href="BoardListServlet">キャンセル</a>
	</form>

</body>
</html>
