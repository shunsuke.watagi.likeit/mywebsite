<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body class="board" style="background-color: lightcyan;">
	<div>
		<font size="6">コメント作成機能</font>
	</div>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right"><font size="5">ログアウト</font></td>
		</p>
	</a>

	<table border="5">
		<tr>

			<th>ユーザ名</th>
			<th>タイトル</th>
			<th>タグ名</th>
			<th width="80%">本文</th>

		</tr>

		<tr>


			<td>${Board.name}</td>
			<td>${Board.title}</td>
			<td>${Board.tagName}</td>
			<td>${Board.main}</td>

		</tr>

	</table>
	<form class="form-horizontal" action="CommentMakeServlet" method="post">
		<div class="form-groups col-sm-3">
			<p>ユーザ名</p>
			<input type="text" class="form-control" name="name" size="20"
				maxlength="20" value="${userInfo.userName}">
		</div>
		<div class="form-groups col-sm-3">
			<p>タイトル</p>
			<input type="text" class="form-control" name="title" size="20"
				maxlength="20">
		</div>
		<div class="form-groups col-sm-3">
			<p>本文</p>
			<textarea type="text" class="form-control" name="main" rows="20"
				cols="20"> </textarea>
		</div>
		<input type="hidden" name="userId" value="${userInfo.userId}">
		<input type="hidden" name="boardId" value="${Board.id}">
		<div>
			<button type="submit" class="btn btn-primary col-sm-1"
				style="width: 6%;">投稿</button>
		</div>
		<a class="nav-link" href="BoardListServlet">キャンセル</a>
	</form>


</body>

</html>
