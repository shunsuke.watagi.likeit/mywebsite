<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board">
	<div class="headline">
		<font size="6">投稿検索画面</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right"><font size="5">ログアウト</font></td>
		</p>
	</a>

	<form style="text-align: center" class="form-horizontal"
		action="BoardSearchServlet" method="post">
		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<div class="form-group col-sm-3">
			<p>ユーザ名検索（部分一致）</p>
			<input type="text" name="name" size="20" maxlength="20"
				class="form-control">
		</div>
		<div class="form-group col-sm-3">
			<p>本文検索（部分一致）</p>
			<input type="text" class="form-control" name="main" rows="20"
				cols="50">
		</div>
		<div class="form-group col-sm-3">
			<p>タグ検索(全一致)</p>
			<input type="text" class="form-control" name="tagName" rows="20"
				cols="50">
		</div>
		<div>
			<button type="submit" class="btn btn-primary"
				style="WIDTH: 80px; HEIGHT: 30px"background-color:#66CCFF;>
				<font>検索</font>
			</button>
		</div>
	</form>






	<table border="4">
		<tr>
			<th>ユーザ名</th>
			<th>タグ名</th>
			<th width="80%">本文</th>
			<th></th>
		</tr>
		<c:forEach var="board" items="${boardList}">
			<tr>

				<td>${board.name}</td>
				<td>${board.tagName}</td>
				<td>${board.main}</td>
				<td><a class="nav-link"
					href="BoardMonitorServlet?Id=${board.id}">参照</a></td>

			</tr>

		</c:forEach>


	</table>

</body>

</html>
