<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board" style="background-color: lightcyan;">
	<div>
		<font size="6">投稿記事削除画面</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right"><font size="5">ログアウト</font></td>
		</p>
	</a>
	<table border="5" width="50%">
		<tr>
			<th>タイトル</th>
			<th>タグ名</th>
			<th>本文</th>
		</tr>

		<tr>

			<td>${Board.title}</td>
			<td>${Board.tagName}</td>
			<td>${Board.main}</td>

		</tr>

	</table>
	<div>
		<font size="5">この記事を削除してよろしいですか？</font>
	</div>

	<table>
		<tr>
			<form action="BoardDeleteServlet" method="post">
				<td><button type="submit"
						class="button8 btn btn-primary btn-lg" name="yes"
						style="width: 100px; height: 50px">はい</button></td> <input
					type="hidden" name="id" value="${Board.id}">
			</form>
			<form action="BoardListServlet" method="get">
				<td><button type="submit"
						class="button8 btn btn-secondary btn-lg" name="no">いいえ</button></td>
			</form>
		</tr>
	</table>


</body>

</html>
