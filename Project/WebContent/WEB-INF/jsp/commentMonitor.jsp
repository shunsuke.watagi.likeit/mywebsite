<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>title</title>
<link href="MyWebSite.css" rel="stylesheet" type="text/css" />
</head>

<body class="board" style="background-color: lightcyan;">
	<div>
		<font size="6">コメント表示</font>
	</div>
	<a class="headline1" href="LogoutServlet">
		<p style="text-align: right">
			<td align="right">ログアウト</td>
		</p>
	</a>



	<table border="5">
		<tr>

			<th>ユーザ名</th>
			<th>タイトル</th>
			<th width="80%">本文</th>

		</tr>

		<tr>
			<td>${comment.cName}</td>
			<td>${comment.cTitle}</td>
			<td>${comment.cMain}</td>
		</tr>


		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>

	<table class="comment">
		<tr>
			<th>ユーザ名</th>
			<th width="80%">コメント</th>
			<th></th>
		</tr>
		<c:forEach var="comment" items="${commentList}">
			<tr>

				<td>${comment.cName}</td>
				<td>${comment.cMain}</td>
				<td><a class="btn btn-primary"
					href="CommentMonitorServlet?id=${comment.cId}">参照</a><a
					class="btn btn-primary"
					href="CommentReMakeServlet?id=${comment.cId}">コメント</a> <c:if
						test="${userInfo.userId==comment.cUserId}">
						<a class="btn btn-success"
							href="CommentDeleteServlet?id=${comment.cId}">削除</a>
					</c:if></td>
			</tr>
		</c:forEach>
	</table>
</body>

</html>