package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BoardDao;
import dao.CommentDao;
import model.Board;

/**
 * Servlet implementation class CommentMakeServlet
 */
@WebServlet("/CommentMakeServlet")
public class CommentMakeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentMakeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String Id = request.getParameter("Id");

		// 確認用：idをコンソールに出力
		System.out.println(Id);

		BoardDao boardDao = new BoardDao();
		Board board = boardDao.findByID(Id);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("Board", board);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/commentMake.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//入力データを受け取る
		String name = request.getParameter("name");
		String main = request.getParameter("main");
		String title = request.getParameter("title");
		String userId = request.getParameter("userId");
		String boardId = request.getParameter("boardId");
		String cId = request.getParameter("cId");

		if (name.equals("") || main.equals("") || title.equals("")) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/commentMake.jsp");
			dispatcher.forward(request, response);
		}
		CommentDao commentDao = new CommentDao();
		commentDao.make(name, main, title, userId, boardId);

		if (cId != null) {
			request.setCharacterEncoding("UTF-8");

			//入力データを受け取る
			String Cname = request.getParameter("name");
			String Cmain = request.getParameter("main");
			String Ctitle = request.getParameter("title");
			String CuserId = request.getParameter("userId");
			String CId = request.getParameter("cId");

			CommentDao CommentDao = new CommentDao();
			commentDao.make(Cname, Cmain, Ctitle, CuserId, CId);
		}
		response.sendRedirect("BoardListServlet");

	}
}
