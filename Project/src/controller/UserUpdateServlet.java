package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String userId = request.getParameter("userId");

		// 確認用：idをコンソールに出力
		System.out.println(userId);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userdao = new UserDao();
		User user = userdao.findByID(userId);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("User", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//入力データを受け取る
		String userId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		String reLoginPassword = request.getParameter("reLoginPassword");
		String loginPassword = request.getParameter("loginPassword");
		String birthDate = request.getParameter("birthDate");
		String loginId = request.getParameter("loginId");

		if (userName.equals("") || birthDate.equals("") || !(loginPassword.equals(reLoginPassword))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			UserDao userdao = new UserDao();
			User user = userdao.findByID(userId);
			// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			request.setAttribute("User", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		UserDao userDao = new UserDao();
		if (loginPassword.equals("") && reLoginPassword.equals("")) {
			userDao.updatePass(userName, birthDate, loginId);
		}

		else {

			userDao.update(userName, loginPassword, birthDate, loginId);
		}

		response.sendRedirect("UserListServlet");

	}

}
