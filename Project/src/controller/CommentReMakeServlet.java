package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CommentDao;
import model.Comment;

/**
 * Servlet implementation class CommentMakeServlet
 */
@WebServlet("/CommentReMakeServlet")
public class CommentReMakeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentReMakeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cId = request.getParameter("id");


		// 確認用：idをコンソールに出力
		System.out.println(cId);

	CommentDao commentDao = new CommentDao();
Comment comment = commentDao.findByID(cId);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("comment", comment);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/commentReMake.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//入力データを受け取る
		String name = request.getParameter("name");
		String main = request.getParameter("main");
		String title = request.getParameter("title");
		String userId = request.getParameter("userId");
		String boardId = request.getParameter("boardId");
		String cId = request.getParameter("cId");
		
		
		if (name.equals("") || main.equals("") || title.equals("")) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardMake.jsp");
			dispatcher.forward(request, response);
		}
		CommentDao commentDao = new CommentDao();
		commentDao.reMake(name, main, title, userId, boardId,cId);


		response.sendRedirect("BoardListServlet");

	}
}
