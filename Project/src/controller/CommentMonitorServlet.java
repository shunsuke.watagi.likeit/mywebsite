package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CommentDao;
import model.Comment;

/**
 * Servlet implementation class BoardMonitorServlet
 */
@WebServlet("/CommentMonitorServlet")
public class CommentMonitorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentMonitorServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String cId = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(cId);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		CommentDao commentdao = new CommentDao();
		Comment comment = commentdao.findByID(cId);

		CommentDao commentDao=new CommentDao();
		List<Comment> commentList=commentDao.reFindAll(cId);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("comment", comment);
		request.setAttribute("commentList", commentList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/commentMonitor.jsp");
		dispatcher.forward(request, response);
	}

}