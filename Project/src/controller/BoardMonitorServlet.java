package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BoardDao;
import dao.CommentDao;
import model.Board;
import model.Comment;

/**
 * Servlet implementation class BoardMonitorServlet
 */
@WebServlet("/BoardMonitorServlet")
public class BoardMonitorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BoardMonitorServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String Id = request.getParameter("Id");

		// 確認用：idをコンソールに出力
		System.out.println(Id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		BoardDao boarddao = new BoardDao();
		Board board = boarddao.findByID(Id);

		CommentDao commentDao=new CommentDao();
		List<Comment> commentList=commentDao.findAll(Id);
		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("board", board);
		request.setAttribute("commentList", commentList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardMonitor.jsp");
		dispatcher.forward(request, response);
	}

}
