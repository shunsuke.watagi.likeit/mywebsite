package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BoardDao;
import model.Board;

/**
 * Servlet implementation class BoardSearchServlet
 */
@WebServlet("/BoardSearchServlet")
public class BoardSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BoardSearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardSearch.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//入力データを受け取る
		String name = request.getParameter("name");
		String main = request.getParameter("main");
		String tagName = request.getParameter("tagName");

		if (name.equals("") && main.equals("") && tagName.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "検索条件を入力してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardSearch.jsp");
			dispatcher.forward(request, response);
		} else {
			BoardDao boardDao = new BoardDao();
			List<Board> boardList = boardDao.search(name, main, tagName);
			if (boardList.size() == 0) {// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "該当する投稿がありません");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardSearch.jsp");
				dispatcher.forward(request, response);
			} else {// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("boardList", boardList);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardSearch.jsp");
				dispatcher.forward(request, response);

			}

		}
	}
}
