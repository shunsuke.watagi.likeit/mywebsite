package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BoardDao;
import dao.TagDao;
import model.Board;

/**
 * Servlet implementation class BoardMakeServlet
 */
@WebServlet("/BoardMakeServlet")
public class BoardMakeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BoardMakeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardMake.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//入力データを受け取る
		String name = request.getParameter("name");
		String title = request.getParameter("title");
		String main = request.getParameter("main");
		String tagName = request.getParameter("tagName");
		String userId = request.getParameter("userId");

		if (name.equals("") || title.equals("") || main.equals("")
				|| tagName.equals("")) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "投稿内容を確認してください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/boardMake.jsp");
			dispatcher.forward(request, response);
		} else {

			BoardDao boardDao = new BoardDao();
			boardDao.make(name, title, main, tagName, userId);
			Board board = boardDao.findByTagName(tagName);
			int id = board.getId();
			TagDao tagDao = new TagDao();
			tagDao.make(tagName, id);

			response.sendRedirect("BoardListServlet");

		}

	}
}
