package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Comment;

public class CommentDao {
	public void make(String name, String main, String title, String userId, String boardId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに登録する
			String sql = "insert into comment(cMain,cName,cTitle,cCreateDate,cUserId,cBoardId)values(?,?,?,now(),?,?)";
			PreparedStatement pStmt;
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, main);
			pStmt.setString(2, name);
			pStmt.setString(3, title);
			pStmt.setString(4, userId);
			pStmt.setString(5, boardId);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public List<Comment> findAll(String cBoardId) {
		Connection conn = null;
		List<Comment> commentList = new ArrayList<Comment>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM comment WHERE cBoardId=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, cBoardId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int cId = rs.getInt("cId");
				String cMain = rs.getString("cMain");
				String cName = rs.getString("cName");
				String cTitle = rs.getString("cTitle");
				String cCreateDate = rs.getString("cCreateDate");
				String cUserId = rs.getString("cUserId");
				String cBoardId1 = rs.getString("cBoardId");
				String cTocId1 = rs.getString("cTocId");
				Comment comment = new Comment(cId, cMain, cName, cTitle, cCreateDate, cUserId, cBoardId1, cTocId1);

				commentList.add(comment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return commentList;
	}

	public List<Comment> reFindAll(String cTocId) {
		Connection conn = null;
		List<Comment> commentList = new ArrayList<Comment>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM comment WHERE cTocId=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, cTocId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int cId = rs.getInt("cId");
				String cMain = rs.getString("cMain");
				String cName = rs.getString("cName");
				String cTitle = rs.getString("cTitle");
				String cCreateDate = rs.getString("cCreateDate");
				String cUserId = rs.getString("cUserId");
				String cBoardId = rs.getString("cBoardId");
				String cTocId1 = rs.getString("cTocId");

				Comment comment = new Comment(cId, cMain, cName, cTitle, cCreateDate, cUserId, cBoardId, cTocId1);

				commentList.add(comment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return commentList;
	}

	public Comment findByID(String cId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "select *from comment join board on board.Id=comment.cBoardId where cId=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, cId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int cIdData = rs.getInt("cId");
			String cMainData = rs.getString("cMain");
			String cNameData = rs.getString("cName");
			String cTitleData = rs.getString("cTitle");
			String cCreateDateData = rs.getString("cCreateDate");
			String cUserIdData = rs.getString("cUserId");
			String cBoardIdData = rs.getString("cBoardId");
			int IdData = rs.getInt("Id");
			return new Comment(cIdData, cMainData, cNameData, cTitleData, cCreateDateData, cUserIdData, cBoardIdData,IdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void reMake(String name, String main, String title, String userId, String boardId, String cId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに登録する
			String sql = "insert into comment(cMain,cName,cTitle,cCreateDate,cUserId,cBoardId,cTocId)values(?,?,?,now(),?,?,?)";
			PreparedStatement pStmt;
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, main);
			pStmt.setString(2, name);
			pStmt.setString(3, title);
			pStmt.setString(4, userId);
			pStmt.setString(5, boardId);
			pStmt.setString(6, cId);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void delete(String cId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//選択されたデータをDBから削除する

			String sql = "delete  from comment where cId=?";

			PreparedStatement pStmt;

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, cId);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
