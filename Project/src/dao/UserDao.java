package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	public UserDao() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @return
	 */
	public User findByLoginInfo(String loginId, String loginPassword) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE loginId = ? and loginPassword = ?";

			// SELECTを実行し、結果表を取得
			String Result = cry(loginPassword);
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, Result);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int userIdData = rs.getInt("userId");
			String loginIdData = rs.getString("loginId");
			String userNameData = rs.getString("userName");
			return new User(userIdData, loginIdData, userNameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int userId = rs.getInt("userId");
				String loginId = rs.getString("loginId");
				String userName = rs.getString("userName");
				Date birthDate = rs.getDate("birthDate");
				String loginPassword = rs.getString("loginPassword");
				String createDate = rs.getString("createDate");
				String updateDate = rs.getString("updateDate");
				User user = new User(userId, loginId, userName, birthDate, loginPassword, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 *新規登録したデータをDBに登録
	 */
	public void register(String loginId, String loginPassword, String userName, String birthDate) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに登録する
			String sql = "insert into user(loginId,loginPassword,username,createDate,updateDate,birthDate)values(?,?,?,now(),now(),?)";
			PreparedStatement pStmt;
			String Result = cry(loginPassword);
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, Result);
			pStmt.setString(3, userName);
			pStmt.setString(4, birthDate);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByID(String userId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE userId=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int userIdData = rs.getInt("userId");
			String loginIdData = rs.getString("loginId");
			String userNameData = rs.getString("userName");
			Date birthDateData = rs.getDate("birthDate");
			String createdateData = rs.getString("createDate");
			String updateData = rs.getString("updateDate");
			return new User(userIdData, loginIdData, userNameData, birthDateData, createdateData, updateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 *入力した情報を更新
	 */
	public void update(String userName, String loginPassword, String birthDate, String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに更新する

			String sql = "update user set userName=?, loginPassword=?, birthDate=? where loginId=?";

			PreparedStatement pStmt;
			String Result = cry(loginPassword);
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, Result);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, loginId);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void updatePass(String userName, String birthDate, String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに更新する

			String sql = "update user set userName=?, birthDate=? where loginId=?";

			PreparedStatement pStmt;

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, loginId);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void delete(String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//選択されたデータをDBから削除する

			String sql = "delete from user where loginId=?";

			PreparedStatement pStmt;

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> search(String loginIdP, String nameP, String birth_dateP, String birth_date2P) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE userId != 1";

			if (!loginIdP.equals("")) {
				sql += " AND loginId = '" + loginIdP + "'";
			}

			if (!nameP.equals("")) {
				sql += " AND userName like '%" + nameP + "%'";
			}

			if (!birth_dateP.equals("")) {
				sql += " AND birthDate >='" + birth_dateP + "'";
			}

			if (!birth_date2P.equals("")) {
				sql += " AND birthDate <= '" + birth_date2P + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("loginId");
				String userName = rs.getString("userName");
				Date birthDate = rs.getDate("birthDate");
				User user = new User(loginId, userName, birthDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE loginId = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("loginId");
			String userNameData = rs.getString("userName");
			return new User(loginIdData, userNameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String cry(String loginPassword) {
		//ハッシュを生成したい元の文字列
		String source = loginPassword;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;

	}
}
