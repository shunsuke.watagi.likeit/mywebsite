package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TagDao {
	public void make(String tagName, int id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに登録する
			String sql = "insert into tag(tagName,boardId)values(?,?)";
			PreparedStatement pStmt;
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, tagName);
			pStmt.setInt(2, id);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
