package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Board;

public class BoardDao {

	public void delete(String Id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//選択されたデータをDBから削除する

			String sql = "delete  from board where Id=?";

			PreparedStatement pStmt;

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);
			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public Board findByID(String Id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "select *from board where Id=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int IdData = rs.getInt("Id");
			String mainData = rs.getString("main");
			String nameData = rs.getString("name");
			String titleData = rs.getString("title");
			String tagNameData = rs.getString("tagName");
			String createDateData = rs.getString("createDate");
			String userIdData = rs.getString("userId");

			return new Board(IdData, mainData, nameData, titleData, createDateData, userIdData, tagNameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<Board> findAll() {
		Connection conn = null;
		List<Board> boardList = new ArrayList<Board>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "select *from board";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("Id");
				String main = rs.getString("main");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String createDate = rs.getString("createDate");
				String userId = rs.getString("userId");
				String tagName = rs.getString("tagName");
				Board board = new Board(id, main, name, title, createDate, userId, tagName);

				boardList.add(board);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return boardList;
	}

	public void make(String name, String title, String main, String tagName, String userId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//登録されたデータをDBに登録する
			String sql = "insert into board(main,name,title,createDate,userId,tagName)values(?,?,?,now(),?,?)";
			PreparedStatement pStmt;
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, main);
			pStmt.setString(2, name);
			pStmt.setString(3, title);
			pStmt.setString(4, userId);
			pStmt.setString(5, tagName);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public List<Board> search(String name, String main, String tagName) {
		Connection conn = null;
		List<Board> boardList = new ArrayList<Board>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM board where Id != 0";

			if (!name.equals("")) {
				sql += " and name like '%" + name + "%'";
			}

			if (!main.equals("")) {
				sql += " and main like '%" + main + "%'";
			}

			if (!tagName.equals("")) {
				sql += " and tagName ='" + tagName + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id1 = rs.getInt("Id");
				String name1 = rs.getString("name");
				String main1 = rs.getString("main");
				String tagName1 = rs.getString("tagName");
				String createDate1 = rs.getString("createDate");

				Board board = new Board(id1,name1, main1, tagName1, createDate1);

				boardList.add(board);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return boardList;
	}
	public Board findByTagName(String tagName) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "select *from board where tagName=?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, tagName);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int IdData = rs.getInt("Id");

			return new Board(IdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
