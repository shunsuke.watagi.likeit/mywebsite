package model;

import java.io.Serializable;

public class Board implements Serializable {

	private int Id;
	private String main;
	private String name;
	private String title;
	private String createDate;
	private String userId;
	private String tagName;

	public Board(int IdData, String mainData, String nameData, String titleData, String createDateData,
			String userIdData, String tagNameData, String main1Data, String name1Data, String title1Data, String createDate1Data, String userId1Data) {
		this.Id = IdData;
		this.main = mainData;
		this.name = nameData;
		this.title = titleData;
		this.createDate = createDateData;
		this.userId = userIdData;
		this.tagName = tagNameData;

	}

	public Board(String name, String main, String tagName, String createDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.name= name;
		this.main = main;
		this.tagName = tagName;
		this.createDate = createDate;
	}



	public Board(int id2, String main2, String name2, String title2, String createDate2, String userId2,
			String tagName2) {
		this.Id = id2;
		this.main = main2;
		this.name = name2;
		this.title = title2;
		this.createDate = createDate2;
		this.userId = userId2;
		this.tagName = tagName2;

	}

	public Board(int id2) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.Id=id2;
	}

	public Board(int id1, String name1, String main1, String tagName1, String createDate1) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.Id = id1;
		this.main = main1;
		this.name = name1;
		this.createDate = createDate1;
		this.tagName = tagName1;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getId() {
		return Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
