package model;

import java.io.Serializable;

public class Comment implements Serializable {

	private int cId;
	private String cMain;
	private String cName;
	private String cTitle;
	private String cCreateDate;
	private String cUserId;
	private String cTagName;
	private String cBoardId;
	private String cTocId;
	private int id;

	public Comment(int cId, String cMain, String cName, String cTitle, String cCreateDate, String cUserId,
			String cBoardId) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.cId = cId;
		this.cMain = cMain;
		this.cName = cName;
		this.cTitle = cTitle;
		this.cCreateDate = cCreateDate;
		this.cUserId = cUserId;
		this.cBoardId = cBoardId;

	}

	public String getcTocId() {
		return cTocId;
	}

	public void setcTocId(String cTocId) {
		this.cTocId = cTocId;
	}

	public Comment(int cId2, String cMain2, String cName2, String cTitle2, String cCreateDate2, String cUserId2,
			String cBoardId2, String cTocId) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.cId = cId2;
		this.cMain = cMain2;
		this.cName = cName2;
		this.cTitle = cTitle2;
		this.cCreateDate = cCreateDate2;
		this.cUserId = cUserId2;
		this.cBoardId = cBoardId2;
		this.cTocId = cTocId;

	}

	public Comment(int cIdData, String cMainData, String cNameData, String cTitleData, String cCreateDateData,
			String cUserIdData, String cBoardIdData, int idData) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.cId = cIdData;
		this.cMain = cMainData;
		this.cName = cNameData;
		this.cTitle = cTitleData;
		this.cCreateDate = cCreateDateData;
		this.cUserId = cUserIdData;
		this.cBoardId = cBoardIdData;
		this.id = idData;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}

	public String getcMain() {
		return cMain;
	}

	public void setcMain(String cMain) {
		this.cMain = cMain;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcTitle() {
		return cTitle;
	}

	public void setcTitle(String cTitle) {
		this.cTitle = cTitle;
	}

	public String getcCreateDate() {
		return cCreateDate;
	}

	public void setcCreateDate(String cCreateDate) {
		this.cCreateDate = cCreateDate;
	}

	public String getcUserId() {
		return cUserId;
	}

	public void setcUserId(String cUserId) {
		this.cUserId = cUserId;
	}

	public String getcTagName() {
		return cTagName;
	}

	public void setcTagName(String cTagName) {
		this.cTagName = cTagName;
	}

	public String getcBoardId() {
		return cBoardId;
	}

	public void setcBoardId(String cBoardId) {
		this.cBoardId = cBoardId;
	}

}
