package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {
	private int userId;
	private String loginId;
	private String userName;
	private Date birthDate;
	private String loginPassword;
	private String createDate;
	private String updateDate;

	// 一覧検索用
	public User(String loginId, String userName, Date birthDate) {
		this.loginId = loginId;
		this.userName = userName;
		this.birthDate = birthDate;
	}

	// 全てのデータをセットするコンストラクタ
	public User(int userId, String loginId, String userName, Date birthDate, String loginPassword, String createDate,
			String updateDate) {
		this.userId = userId;
		this.loginId = loginId;
		this.userName = userName;
		this.birthDate = birthDate;
		this.loginPassword = loginPassword;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	public User(int userId, String loginId, String userName, Date birthDate, String createDate,
			String updateDate) {
		this.userId = userId;
		this.loginId = loginId;
		this.userName = userName;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;

	}

	//ログインセッションを保存するためのコンストラクタ
	public User(String loginId, String loginPassword) {
		this.loginId = loginId;
		this.loginPassword=loginPassword;
	}

	public User(int userIdData, String loginIdData, String userNameData) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.userId = userIdData;
		this.loginId = loginIdData;
		this.userName = userNameData;
	}

	//詳細表示
	public User(String loginIdData, String userNameData, java.sql.Date birthDateData, String createdateData,
			String updateData) {
		this.loginId = loginIdData;
		this.userName = userNameData;
		this.birthDate = birthDateData;
		this.createDate = createdateData;
		this.updateDate = updateData;
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getLoginPassword() {
		return loginPassword;
	}

	public void setPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
